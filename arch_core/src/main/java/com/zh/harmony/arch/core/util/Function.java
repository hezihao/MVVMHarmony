package com.zh.harmony.arch.core.util;

/**
 * 转换函数接口，I是输入类型，O是输出类型
 */
public interface Function<I, O> {
    /**
     * 转换
     *
     * @param input 输入数据
     * @return 输出数据
     */
    O apply(I input);
}