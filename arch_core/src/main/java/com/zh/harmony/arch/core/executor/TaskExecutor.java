package com.zh.harmony.arch.core.executor;

/**
 * 任务执行器
 */
public abstract class TaskExecutor {
    /**
     * IO线程中执行Runnable
     */
    public abstract void executeOnDiskIO(Runnable runnable);

    /**
     * 主线程执行Runnable
     */
    public abstract void postToMainThread(Runnable runnable);

    /**
     * 主线程执行Runnable，如果当前就是主线程，直接执行，否则转发到主线程再执行
     */
    public void executeOnMainThread(Runnable runnable) {
        if (isMainThread()) {
            runnable.run();
        } else {
            postToMainThread(runnable);
        }
    }

    /**
     * 判断当前是否是主线程
     */
    public abstract boolean isMainThread();
}