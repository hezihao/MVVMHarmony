package com.zh.harmony.livedata;

/**
 * MutableLiveData，只是把LiveData的postValue()和setValue()，访问权限改为public
 */
@SuppressWarnings("WeakerAccess")
public class MutableLiveData<T> extends LiveData<T> {
    /**
     * 给定一个值，创建并初始化MutableLiveData
     */
    public MutableLiveData(T value) {
        super(value);
    }

    /**
     * 创建一个没有赋值的MutableLiveData
     */
    public MutableLiveData() {
        super();
    }

    @Override
    public void postValue(T value) {
        super.postValue(value);
    }

    @Override
    public void setValue(T value) {
        super.setValue(value);
    }
}