package com.zh.harmony.livedata;

/**
 * LiveData的观察者接口
 */
public interface Observer<T> {
    /**
     * 数据变化时，回调
     */
    void onChanged(T t);
}