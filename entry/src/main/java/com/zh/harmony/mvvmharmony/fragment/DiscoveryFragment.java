package com.zh.harmony.mvvmharmony.fragment;

import com.zh.harmony.fragment.Fragment;
import com.zh.harmony.mvvmharmony.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * 发现
 */
public class DiscoveryFragment extends Fragment {
    @Override
    protected Component onCreateView(LayoutScatter inflater, ComponentContainer container) {
        return inflater.parse(ResourceTable.Layout_fragment_discovery, container, false);
    }
}