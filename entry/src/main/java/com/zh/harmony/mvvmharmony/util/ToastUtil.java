package com.zh.harmony.mvvmharmony.util;

import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class ToastUtil {
    private ToastUtil() {
    }

    public static void toast(Context context, String msg) {
        if (context == null) {
            return;
        }
        if (TextUtils.isEmpty(msg)) {
            return;
        }
        Runnable action = new Runnable() {
            @Override
            public void run() {
                new ToastDialog(context.getApplicationContext())
                        .setText(msg)
                        .setAlignment(LayoutAlignment.BOTTOM)
                        .show();
            }
        };
        if (EventRunner.getMainEventRunner().getEventQueue() == EventRunner.getCurrentEventQueue()) {
            //当前就是主线程，直接执行
            action.run();
        } else {
            //其他线程，通过EventHandler发送到主线程中执行
            new EventHandler(EventRunner.getMainEventRunner())
                    .postTask(action);
        }
    }
}