package com.zh.harmony.mvvmharmony.ability;

import com.zh.harmony.lifecycle.*;
import com.zh.harmony.mvvmharmony.base.BaseAbility;
import com.zh.harmony.mvvmharmony.slice.MainSlice;
import com.zh.harmony.mvvmharmony.util.HiLogUtil;
import ohos.aafwk.content.Intent;

/**
 * 主页
 */
public class MainAbility extends BaseAbility {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainSlice.class.getName());
        //统一回调一个方法的监听器的方式
//        getLifeCycle().addObserver(new LifecycleEventObserver() {
//            @Override
//            public void onStateChanged(LifecycleOwner source, Lifecycle.Event event) {
//                HiLogUtil.debug(event.name());
//            }
//        });
        //拆分回调具体的方法的形式
//        getLifeCycle().addObserver(new FullLifecycleObserver() {
//            @Override
//            public void onCreate(LifecycleOwner owner) {
//                HiLogUtil.debug("ON_CREATE");
//            }
//
//            @Override
//            public void onStart(LifecycleOwner owner) {
//                HiLogUtil.debug("ON_START");
//            }
//
//            @Override
//            public void onResume(LifecycleOwner owner) {
//                HiLogUtil.debug("ON_RESUME");
//            }
//
//            @Override
//            public void onPause(LifecycleOwner owner) {
//                HiLogUtil.debug("ON_PAUSE");
//            }
//
//            @Override
//            public void onStop(LifecycleOwner owner) {
//                HiLogUtil.debug("ON_STOP");
//            }
//
//            @Override
//            public void onDestroy(LifecycleOwner owner) {
//                HiLogUtil.debug("ON_DESTROY");
//            }
//        });
        //Java8默认方法的方式
        getLifeCycle().addObserver(new DefaultLifecycleObserver() {
            @Override
            public void onCreate(LifecycleOwner owner) {
                HiLogUtil.debug("ON_CREATE");
            }

            @Override
            public void onStart(LifecycleOwner owner) {
                HiLogUtil.debug("ON_START");
            }

            @Override
            public void onResume(LifecycleOwner owner) {
                HiLogUtil.debug("ON_RESUME");
            }

            @Override
            public void onPause(LifecycleOwner owner) {
                HiLogUtil.debug("ON_PAUSE");
            }

            @Override
            public void onStop(LifecycleOwner owner) {
                HiLogUtil.debug("ON_STOP");
            }

            @Override
            public void onDestroy(LifecycleOwner owner) {
                HiLogUtil.debug("ON_DESTROY");
            }
        });
        //反射注解方法的方式
        //getLifeCycle().addObserver(new LifecycleLogObserver());
    }

    private static class LifecycleLogObserver implements LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        public void onCreate() {
            HiLogUtil.debug("ON_CREATE");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void onStart() {
            HiLogUtil.debug("ON_START");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        public void onResume() {
            HiLogUtil.debug("ON_RESUME");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        public void onPause() {
            HiLogUtil.debug("ON_PAUSE");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void onStop() {
            HiLogUtil.debug("ON_STOP");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void onDestroy() {
            HiLogUtil.debug("ON_DESTROY");
        }
    }
}