package com.zh.harmony.mvvmharmony.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Log工具类
 */
public class HiLogUtil {
    private static final HiLogLabel Default_Label = new HiLogLabel(HiLog.LOG_APP, 0, "HiLogUtil");
    private static final String DEFAULT_TAG = "HiLogUtil";

    public static int error(String format) {
        return HiLogUtil.error(0, format);
    }

    public static int debug(String format) {
        return HiLogUtil.debug(0, format);
    }

    public static int info(String format) {
        return HiLogUtil.info(0, format);
    }

    public static int warn(String format) {
        return HiLogUtil.warn(0, format);
    }

    public static int fatal(String format) {
        return HiLogUtil.fatal(0, format);
    }

    public static int error(int domain, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, DEFAULT_TAG);
        return HiLogUtil.error(logLabel, format);
    }

    public static int debug(int domain, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, DEFAULT_TAG);
        return HiLogUtil.debug(logLabel, format);
    }

    public static int info(int domain, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, DEFAULT_TAG);
        return HiLogUtil.info(logLabel, format);
    }

    public static int warn(int domain, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, DEFAULT_TAG);
        return HiLogUtil.warn(logLabel, format);
    }

    public static int fatal(int domain, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, DEFAULT_TAG);
        return HiLogUtil.fatal(logLabel, format);
    }

    public static int error(int domain, String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, TAG);
        return HiLogUtil.error(logLabel, format);
    }

    public static int debug(int domain, String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, TAG);
        return HiLogUtil.debug(logLabel, format);
    }

    public static int info(int domain, String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, TAG);
        return HiLogUtil.info(logLabel, format);
    }

    public static int warn(int domain, String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, TAG);
        return HiLogUtil.warn(logLabel, format);
    }

    public static int fatal(int domain, String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, domain, TAG);
        return HiLogUtil.fatal(logLabel, format);
    }


    public static int error(String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
        return HiLogUtil.error(logLabel, format);
    }

    public static int debug(String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
        return HiLogUtil.debug(logLabel, format);
    }

    public static int info(String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
        return HiLogUtil.info(logLabel, format);
    }

    public static int warn(String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
        return HiLogUtil.warn(logLabel, format);
    }

    public static int fatal(String TAG, String format) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
        return HiLogUtil.fatal(logLabel, format);
    }


    public static int error(String format, Object... args) {

        return HiLogUtil.error(Default_Label, format, args);
    }

    public static int debug(String format, Object... args) {

        return HiLogUtil.debug(Default_Label, format, args);
    }

    public static int info(String format, Object... args) {

        return HiLogUtil.info(Default_Label, format, args);
    }

    public static int warn(String format, Object... args) {

        return HiLogUtil.warn(Default_Label, format, args);
    }

    public static int fatal(String format, Object... args) {

        return HiLogUtil.fatal(Default_Label, format, args);
    }


    public static int error(HiLogLabel label, String format, Object... args) {
        return HiLog.error(label, format, args);
    }

    public static int debug(HiLogLabel label, String format, Object... args) {
        return HiLog.debug(label, format, args);
    }

    public static int info(HiLogLabel label, String format, Object... args) {

        return HiLog.info(label, format, args);
    }

    public static int warn(HiLogLabel label, String format, Object... args) {

        return HiLog.warn(label, format, args);
    }

    public static int fatal(HiLogLabel label, String format, Object... args) {

        return HiLog.fatal(label, format, args);
    }
}