package com.zh.harmony.mvvmharmony.ability;

import com.zh.harmony.mvvmharmony.base.BaseAbility;
import com.zh.harmony.mvvmharmony.slice.LoginSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * 登录
 */
public class LoginAbility extends BaseAbility {
    /**
     * Ability跳转
     */
    public static void start(Ability ability) {
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(ability.getBundleName())
                .withAbilityName(LoginAbility.class.getName())
                .build();
        Intent jumpIntent = new Intent();
        jumpIntent.setOperation(operation);
        ability.startAbility(jumpIntent);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(LoginSlice.class.getName());
    }
}