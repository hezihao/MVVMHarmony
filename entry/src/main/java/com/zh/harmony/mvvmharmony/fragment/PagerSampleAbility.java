package com.zh.harmony.mvvmharmony.fragment;

import com.zh.harmony.mvvmharmony.ResourceTable;
import com.zh.harmony.mvvmharmony.base.BaseAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * PagerSlider页面
 */
public class PagerSampleAbility extends BaseAbility {
    public static void start(Ability ability) {
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(ability.getBundleName())
                .withAbilityName(PagerSampleAbility.class.getName())
                .build();
        Intent intent = new Intent();
        intent.setOperation(operation);
        ability.startAbility(intent);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pager_sample);
        getFragmentManager().beginTransaction()
                .replace(ResourceTable.Id_container, HomeFragment.newInstance(), HomeFragment.class.getName())
                .commit();
    }
}