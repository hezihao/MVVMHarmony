package com.zh.harmony.mvvmharmony;

import com.ryan.ohos.extension.event.impl.ViewGroupHelper;
import com.ryan.ohos.extension.event.interfaces.ViewGroup;
import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 帧布局
 */
public class FrameLayout extends StackLayout implements ViewGroup {
    private final ViewGroupHelper mViewGroupHelper;

    public FrameLayout(Context context) {
        this(context, null);
    }

    public FrameLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public FrameLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.mViewGroupHelper = new ViewGroupHelper(this);
    }

    @Override
    public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        this.mViewGroupHelper.requestDisallowInterceptTouchEvent(disallowIntercept);
    }

    @Override
    public boolean onInterceptTouchEvent(TouchEvent touchEvent) {
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(TouchEvent touchEvent) {
        return this.mViewGroupHelper.dispatchTouchEvent(touchEvent);
    }

    @Override
    public boolean onTouchEvent(TouchEvent touchEvent) {
        return false;
    }

    @Override
    public boolean isConsumed() {
        return this.mViewGroupHelper.isConsumed();
    }
}