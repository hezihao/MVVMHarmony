package com.zh.harmony.mvvmharmony;

/**
 * 常量类
 */
public class Constant {
    private Constant() {
    }

    /**
     * 用户名
     */
    public static final String KEY_USER_NAME = "key_user_name";

    /**
     * 密码
     */
    public static final String KEY_PASSWORD = "key_password";

    /**
     * 登录成功
     */
    public static final String ACTION_LOGIN_SUCCESS = "action_login_success";
}