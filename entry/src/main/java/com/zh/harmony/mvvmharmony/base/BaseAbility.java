package com.zh.harmony.mvvmharmony.base;

import com.zh.harmony.fragment.FragmentAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * Ability基类
 */
public abstract class BaseAbility extends FragmentAbility {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.TRANSPARENT.getValue());
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
    }
}