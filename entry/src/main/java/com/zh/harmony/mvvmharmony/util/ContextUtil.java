package com.zh.harmony.mvvmharmony.util;

import ohos.app.Context;

/**
 * Context工具类
 */
public class ContextUtil {
    private static Context mContext;

    private ContextUtil() {
    }

    /**
     * 保存Context
     */
    public static void attachContext(Context context) {
        mContext = context.getApplicationContext();
    }

    /**
     * 获取Context
     */
    public static Context getContext() {
        return mContext;
    }
}