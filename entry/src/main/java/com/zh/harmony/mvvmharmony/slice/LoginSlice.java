package com.zh.harmony.mvvmharmony.slice;

import com.ryan.ohos.extension.widget.toolbar.Toolbar;
import com.zh.harmony.livedata.Observer;
import com.zh.harmony.livedatabus.LiveDataBus;
import com.zh.harmony.mvvm.ViewModelProviders;
import com.zh.harmony.mvvm.ability.ComponentAbility;
import com.zh.harmony.mvvm.slice.ComponentAbilitySlice;
import com.zh.harmony.mvvmharmony.Constant;
import com.zh.harmony.mvvmharmony.ResourceTable;
import com.zh.harmony.mvvmharmony.model.UserInfo;
import com.zh.harmony.mvvmharmony.util.PreferencesUtil;
import com.zh.harmony.mvvmharmony.util.TextUtils;
import com.zh.harmony.mvvmharmony.util.ToastUtil;
import com.zh.harmony.mvvmharmony.viewmodel.LoginViewModel;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;

/**
 * 登录页面
 */
public class LoginSlice extends ComponentAbilitySlice {
    private Toolbar vToolbar;
    private Text vInputText;
    private TextField vUsernameTextField;
    private TextField vPasswordTextField;
    private Button vSubmitBtn;

    private LoginViewModel mLoginViewModel;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_login);
        initViewModel();
        findView();
        bindView();
        setData();
    }

    /**
     * 初始化ViewModel
     */
    private void initViewModel() {
        mLoginViewModel = ViewModelProviders.of((ComponentAbility) getAbility())
                .get(LoginViewModel.class);
    }

    private void findView() {
        vToolbar = (Toolbar) findComponentById(ResourceTable.Id_toolbar);
        vInputText = (Text) findComponentById(ResourceTable.Id_input_text);
        vUsernameTextField = (TextField) findComponentById(ResourceTable.Id_username_text_field);
        vPasswordTextField = (TextField) findComponentById(ResourceTable.Id_password_text_field);
        vSubmitBtn = (Button) findComponentById(ResourceTable.Id_submit_button);
    }

    private void bindView() {
        vToolbar.setTitle("Login");
        vToolbar.setNavigationOnClickListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                getAbility().terminateAbility();
            }
        });
        vUsernameTextField.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String text, int i, int i1, int i2) {
                mLoginViewModel.mUserNameLiveData.setValue(text);
            }
        });
        vPasswordTextField.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String text, int i, int i1, int i2) {
                mLoginViewModel.mPasswordLiveData.setValue(text);
            }
        });
        vSubmitBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                String userName = mLoginViewModel.mUserNameLiveData.getValue();
                String password = mLoginViewModel.mPasswordLiveData.getValue();
                if (TextUtils.isEmpty(userName)) {
                    ToastUtil.toast(getContext(), "用户名不能为空");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    ToastUtil.toast(getContext(), "密码不能为空");
                    return;
                }
                //保存用户信息
                PreferencesUtil.putString(Constant.KEY_USER_NAME, userName);
                PreferencesUtil.putString(Constant.KEY_PASSWORD, password);
                //发送登录成功事件
                LiveDataBus.get().with(Constant.ACTION_LOGIN_SUCCESS, UserInfo.class)
                        .setValue(new UserInfo(userName, password));
            }
        });
    }

    private void setData() {
        //读取保存的值
        String userName = PreferencesUtil.getString(Constant.KEY_USER_NAME, "");
        String password = PreferencesUtil.getString(Constant.KEY_PASSWORD, "");
        if (!TextUtils.isEmpty(userName)) {
            mLoginViewModel.mUserNameLiveData.setValue(userName);
        }
        if (!TextUtils.isEmpty(password)) {
            mLoginViewModel.mPasswordLiveData.setValue(password);
        }
        //监听变化
        mLoginViewModel.mUserNameLiveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String newUserName) {
                String currentPassword = mLoginViewModel.mPasswordLiveData.getValue();
                if (TextUtils.isEmpty(currentPassword)) {
                    currentPassword = "";
                }
                vInputText.setText("用户名：" + newUserName + " 密码：" + currentPassword);
                vUsernameTextField.setText(newUserName);
            }
        });
        mLoginViewModel.mPasswordLiveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String newPassword) {
                String currentUserName = mLoginViewModel.mUserNameLiveData.getValue();
                if (TextUtils.isEmpty(currentUserName)) {
                    currentUserName = "";
                }
                vInputText.setText("用户名：" + currentUserName + " 密码：" + newPassword);
                vPasswordTextField.setText(newPassword);
            }
        });
    }
}