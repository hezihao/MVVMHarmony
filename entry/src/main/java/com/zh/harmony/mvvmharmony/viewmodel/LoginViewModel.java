package com.zh.harmony.mvvmharmony.viewmodel;

import com.zh.harmony.livedata.MutableLiveData;
import com.zh.harmony.viewmodel.ViewModel;

/**
 * 登录ViewModel
 */
public class LoginViewModel extends ViewModel {
    /**
     * 用户名
     */
    public final MutableLiveData<String> mUserNameLiveData = new MutableLiveData<>();
    /**
     * 密码
     */
    public final MutableLiveData<String> mPasswordLiveData = new MutableLiveData<>();
}