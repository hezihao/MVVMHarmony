package com.zh.harmony.mvvmharmony.model;

import java.io.Serializable;

/**
 * 用户信息
 */
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 7237386086304449638L;

    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;

    public UserInfo() {
    }

    public UserInfo(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}