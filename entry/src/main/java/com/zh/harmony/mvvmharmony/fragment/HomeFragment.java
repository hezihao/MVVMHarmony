package com.zh.harmony.mvvmharmony.fragment;

import com.ryan.ohos.extension.widget.toolbar.Toolbar;
import com.ryan.ohos.extension.widget.viewpager.ViewPager;
import com.zh.harmony.fragment.Fragment;
import com.zh.harmony.mvvmharmony.FragmentPagerAdapter;
import com.zh.harmony.mvvmharmony.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TabList;
import ohos.utils.Pair;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    private Toolbar vToolbar;
    private TabList vTabLayout;
    private ViewPager vPager;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    protected Component onCreateView(LayoutScatter inflater, ComponentContainer container) {
        return inflater.parse(ResourceTable.Layout_fragment_home, container, false);
    }

    @Override
    public void onViewCreated(Component view) {
        super.onViewCreated(view);
        findView(view);
        bindView();
    }

    private void findView(Component view) {
        vToolbar = (Toolbar) view.findComponentById(ResourceTable.Id_toolbar);
        vTabLayout = (TabList) view.findComponentById(ResourceTable.Id_tab_layout);
        vPager = (ViewPager) view.findComponentById(ResourceTable.Id_pager);
    }

    private void bindView() {
        vToolbar.setTitle("ViewPager");
        vToolbar.setNavigationOnClickListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                getAbility().terminateAbility();
            }
        });
        ArrayList<Pair<String, Class<? extends Fragment>>> tabInfos = new ArrayList<>();
        tabInfos.add(new Pair<>("直播", LiveFragment.class));
        tabInfos.add(new Pair<>("消息", MessageFragment.class));
        tabInfos.add(new Pair<>("发现", DiscoveryFragment.class));
        for (Pair<String, Class<? extends Fragment>> info : tabInfos) {
            TabList.Tab tab = vTabLayout.new Tab(getContext());
            tab.setText(info.f);
            vTabLayout.addTab(tab);
        }
        vPager.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                try {
                    return tabInfos.get(position).s.newInstance();
                } catch (IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }

            @Override
            public int getCount() {
                return tabInfos.size();
            }
        });
        vPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                //PageSlider滑动切换，切换TabList
                vTabLayout.selectTabAt(position);
            }
        });
        vTabLayout.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                //指示器点击，切换PageSlider
                int position = tab.getPosition();
                vPager.setCurrentItem(position, true);
            }

            @Override
            public void onUnselected(TabList.Tab tab) {
            }

            @Override
            public void onReselected(TabList.Tab tab) {
            }
        });
        //指定缓存数量，不销毁所有页面
        vPager.setOffscreenPageLimit(tabInfos.size());
        //选中第一个
        vPager.setCurrentItem(0);
        vTabLayout.selectTabAt(0);
    }
}