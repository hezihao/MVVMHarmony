package com.zh.harmony.mvvmharmony;

import com.zh.harmony.lifecycle.Lifecycle;
import com.zh.harmony.lifecycle.LifecycleEventObserver;
import com.zh.harmony.lifecycle.process.ProcessLifecycleOwner;
import com.zh.harmony.mvvmharmony.util.ContextUtil;
import com.zh.harmony.mvvmharmony.util.HiLogUtil;
import com.zh.harmony.mvvmharmony.util.VersionUtil;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        ContextUtil.attachContext(this);
        initLifecycle();
        printSystemVersion();
    }

    /**
     * 初始化生命周期
     */
    private void initLifecycle() {
        //前后台监听
        ProcessLifecycleOwner.init(this);
        ProcessLifecycleOwner.get().getLifeCycle().addObserver((LifecycleEventObserver) (source, event) -> {
            if (Lifecycle.Event.ON_START == event) {
                HiLogUtil.debug("应用 => 前台");
            } else if (Lifecycle.Event.ON_STOP == event) {
                HiLogUtil.debug("应用 => 后台");
            }
        });
    }

    private void printSystemVersion() {
        if (VersionUtil.isHarmonyOS()) {
            String harmonyVersion = VersionUtil.getHarmonyVersion();
            HiLogUtil.debug("鸿蒙版本：" + harmonyVersion);
        } else {
            HiLogUtil.debug("不是运行在鸿蒙系统");
        }
    }
}