package com.zh.harmony.fragment;

/**
 * 生命周期事件分发器
 */
public class FragmentLifecycleDispatcher {
    private final FragmentManager mFragmentManager;

    public FragmentLifecycleDispatcher(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    /**
     * 分发onCreate()事件
     */
    void dispatchOnCreate() {
        synchronized (this) {
            for (Fragment fragment : mFragmentManager.mFragments) {
                fragment.performFragmentCreate();
            }
        }
    }

    /**
     * 分发onStart()事件
     */
    void dispatchOnStart() {
        synchronized (this) {
            for (Fragment fragment : mFragmentManager.mFragments) {
                fragment.performFragmentStart();
            }
        }
    }

    /**
     * 分发onResume()事件
     */
    void dispatchOnResume() {
        synchronized (this) {
            for (Fragment fragment : mFragmentManager.mFragments) {
                fragment.performFragmentResume();
            }
        }
    }

    /**
     * 分发onPause()事件
     */
    void dispatchOnPause() {
        synchronized (this) {
            for (Fragment fragment : mFragmentManager.mFragments) {
                fragment.performFragmentPause();
            }
        }
    }

    /**
     * 分发onStop()事件
     */
    void dispatchOnStop() {
        synchronized (this) {
            for (Fragment fragment : mFragmentManager.mFragments) {
                fragment.performFragmentStop();
            }
        }
    }

    /**
     * 分发onDestroy()事件
     */
    void dispatchOnDestroy() {
        synchronized (this) {
            for (Fragment fragment : mFragmentManager.mFragments) {
                fragment.performFragmentDestroy();
            }
        }
    }
}