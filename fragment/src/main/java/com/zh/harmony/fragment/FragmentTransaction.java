package com.zh.harmony.fragment;

/**
 * Fragment的事务抽象
 */
public abstract class FragmentTransaction {
    /**
     * 添加Fragment到View容器中，不设置Tag
     *
     * @param containerId 容器Id
     * @param fragment    Fragment实例
     */
    public abstract FragmentTransaction add(int containerId, Fragment fragment);

    /**
     * 添加Fragment到View容器中
     *
     * @param containerId 容器Id
     * @param fragment    Fragment实例
     * @param tag         唯一Tag标志名
     */
    public abstract FragmentTransaction add(int containerId, Fragment fragment, String tag);

    /**
     * 依附
     */
    public abstract FragmentTransaction attach(Fragment fragment);

    /**
     * 解除依附
     */
    public abstract FragmentTransaction detach(Fragment fragment);

    /**
     * 替换Fragment
     */
    public abstract FragmentTransaction replace(int containerId, Fragment fragment);

    /**
     * 替换Fragment
     */
    public abstract FragmentTransaction replace(int containerId, Fragment fragment, String tag);

    /**
     * 移除Fragment
     */
    public abstract FragmentTransaction remove(Fragment fragment);

    /**
     * 显示Fragment
     */
    public abstract FragmentTransaction show(Fragment fragment);

    /**
     * 隐藏Fragment
     */
    public abstract FragmentTransaction hide(Fragment fragment);

    /**
     * 异步提交事务
     */
    public abstract void commit();

    /**
     * 同步提交事务
     */
    public abstract void commitNow();
}