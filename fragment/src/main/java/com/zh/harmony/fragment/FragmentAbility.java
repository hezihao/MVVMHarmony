package com.zh.harmony.fragment;

import com.zh.harmony.mvvm.ability.ComponentAbility;
import ohos.aafwk.content.Intent;

/**
 * 要使用Fragment，需要使用的Ability
 */
public class FragmentAbility extends ComponentAbility {
    private FragmentManager mFragmentManager;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (mFragmentManager == null) {
            mFragmentManager = new FragmentManager(this);
        } else {
            mFragmentManager.attachFragmentAbility(this);
        }
        mFragmentManager.getFragmentLifecycleDispatcher().dispatchOnCreate();
        mFragmentManager.getFragmentLifecycleDispatcher().dispatchOnStart();
    }

    @Override
    protected void onActive() {
        super.onActive();
        mFragmentManager.getFragmentLifecycleDispatcher().dispatchOnResume();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        mFragmentManager.getFragmentLifecycleDispatcher().dispatchOnPause();
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        mFragmentManager.getFragmentLifecycleDispatcher().dispatchOnStop();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFragmentManager.getFragmentLifecycleDispatcher().dispatchOnDestroy();
    }

    public FragmentManager getFragmentManager() {
        return mFragmentManager;
    }
}