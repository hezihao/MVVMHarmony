package com.zh.harmony.fragment;

import com.zh.harmony.lifecycle.Lifecycle;
import com.zh.harmony.lifecycle.LifecycleOwner;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

/**
 * 类似Fragment的ViewController
 */
public abstract class Fragment implements LifecycleOwner {
    /**
     * 唯一Tag
     */
    String mTag;
    /**
     * 存放Fragment中view的容器Id
     */
    int mContainerId;
    /**
     * 是否已经添加了
     */
    boolean isAdd = false;
    /**
     * 是否可见
     */
    boolean isVisible = false;
    /**
     * 管理器
     */
    private FragmentManager mFragmentManager;
    /**
     * 子Fragment管理器
     */
    private FragmentManager mChildFragmentManager;
    /**
     * 根View
     */
    private Component mRoot;

    @Override
    public Lifecycle getLifeCycle() {
        FragmentAbility ability = getAbility();
        return ability.getLifeCycle();
    }

    protected Component performCreateView(LayoutScatter inflater, ComponentContainer container) {
        mRoot = onCreateView(inflater, container);
        return mRoot;
    }

    /**
     * 创建根View并返回
     */
    protected abstract Component onCreateView(LayoutScatter inflater, ComponentContainer container);

    /**
     * View创建完毕
     */
    public void onViewCreated(Component view) {
    }

    void performFragmentCreate() {
        onCreate();
    }

    void performFragmentStart() {
        onStart();
    }

    public void performFragmentResume() {
        onResume();
    }

    public void performFragmentPause() {
        onPause();
    }

    void performFragmentStop() {
        onStop();
    }

    void performFragmentDestroy() {
        onDestroy();
    }

    public void onAttach(Context context) {
    }

    public void onCreate() {
    }

    public void onStart() {
    }

    public void onResume() {
    }

    public void onPause() {
    }

    public void onStop() {
    }

    public void onDestroy() {
    }

    /**
     * 设置FragmentManager
     */
    void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
        this.mChildFragmentManager = new FragmentManager(fragmentManager, this);
    }

    public FragmentManager getFragmentManager() {
        return mFragmentManager;
    }

    public FragmentManager getChildFragmentManager() {
        return mChildFragmentManager;
    }

    /**
     * 设置是否可见
     *
     * @param visible 是否可见
     */
    void visibleChange(boolean visible) {
        if (visible) {
            onShow();
        } else {
            onHide();
        }
    }

    /**
     * 可见时回调
     */
    protected void onShow() {
    }

    /**
     * 不可见时回调
     */
    protected void onHide() {
    }

    /**
     * 判断是否可见
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * 获取Fragment中的根View
     */
    public Component getView() {
        return mRoot;
    }

    public FragmentAbility getAbility() {
        return mFragmentManager.getAbility();
    }

    public Context getContext() {
        return mFragmentManager.getContext();
    }
}