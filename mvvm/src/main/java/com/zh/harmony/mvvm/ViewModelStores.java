package com.zh.harmony.mvvm;

import com.zh.harmony.mvvm.ability.ComponentAbility;
import com.zh.harmony.mvvm.slice.ComponentAbilitySlice;
import com.zh.harmony.viewmodel.ViewModelStore;

/**
 * ViewModelStore工具类
 */
@SuppressWarnings("WeakerAccess")
@Deprecated
public class ViewModelStores {
    private ViewModelStores() {
    }

    /**
     * 返回ComponentAbility的ViewModelStore
     */
    @Deprecated
    public static ViewModelStore of(ComponentAbility ability) {
        return ability.getViewModelStore();
    }

    /**
     * 返回ComponentAbilitySlice的ViewModelStore
     */
    public static ViewModelStore of(ComponentAbilitySlice slice) {
        return slice.getViewModelStore();
    }
}