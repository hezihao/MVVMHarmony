package com.zh.harmony.mvvm.fraction;

import com.zh.harmony.lifecycle.Lifecycle;
import com.zh.harmony.lifecycle.LifecycleOwner;
import com.zh.harmony.viewmodel.ViewModelStore;
import com.zh.harmony.viewmodel.ViewModelStoreOwner;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;

/**
 * 支持ViewModel的AbilitySlice
 */
public class ComponentFraction extends Fraction implements LifecycleOwner, ViewModelStoreOwner {
    private ViewModelStore mViewModelStore;

    @Override
    public Lifecycle getLifeCycle() {
        return ((LifecycleOwner) getFractionAbility()).getLifeCycle();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Ability ability = getFractionAbility();
        //销毁时，没有发生配置重建时，才清理ViewModelStore
        if (mViewModelStore != null && !ability.isUpdatingConfigurations()) {
            mViewModelStore.clear();
        }
    }

    @Override
    public ViewModelStore getViewModelStore() {
        Ability ability = getFractionAbility();
        if (ability == null) {
            throw new NullPointerException("ability must be not null");
        }
        if (ability.getAbilityPackage() == null) {
            throw new IllegalStateException("Your ability is not yet attached to the "
                    + "AbilityPackage instance. You can't request ViewModel before onStart call.");
        }
        if (mViewModelStore == null) {
            mViewModelStore = new ViewModelStore();
        }
        return mViewModelStore;
    }
}