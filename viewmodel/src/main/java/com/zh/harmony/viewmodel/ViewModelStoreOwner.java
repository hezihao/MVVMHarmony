package com.zh.harmony.viewmodel;

/**
 * 拥有ViewModelStore的类，实现这个接口，用于获取持有的ViewModelStore
 */
@SuppressWarnings("WeakerAccess")
public interface ViewModelStoreOwner {
    /**
     * 返回持有的ViewModelStore
     */
    ViewModelStore getViewModelStore();
}