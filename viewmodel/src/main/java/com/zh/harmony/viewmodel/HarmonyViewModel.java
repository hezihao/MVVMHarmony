package com.zh.harmony.viewmodel;

import ohos.aafwk.ability.AbilityPackage;

/**
 * 要使用AbilityPackage构造的ViewModel，该ViewModel可以获取到AbilityPackage
 */
public class HarmonyViewModel extends ViewModel {
    private final AbilityPackage mApplication;

    public HarmonyViewModel(AbilityPackage application) {
        mApplication = application;
    }

    /**
     * 返回AbilityPackage
     */
    @SuppressWarnings({"TypeParameterUnusedInFormals", "unchecked"})
    public <T extends AbilityPackage> T getApplication() {
        return (T) mApplication;
    }
}