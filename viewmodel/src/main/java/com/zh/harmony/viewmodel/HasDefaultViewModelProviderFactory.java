package com.zh.harmony.viewmodel;

/**
 * 该接口用于返回自定义的ViewModel工厂
 */
public interface HasDefaultViewModelProviderFactory {
    /**
     * 返回自定义ViewModel工厂，如果返回null，则使用默认的NewInstanceFactory
     */
    ViewModelProvider.Factory getDefaultViewModelProviderFactory();
}