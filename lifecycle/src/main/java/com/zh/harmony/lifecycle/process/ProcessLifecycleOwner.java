/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zh.harmony.lifecycle.process;

import com.zh.harmony.lifecycle.Lifecycle;
import com.zh.harmony.lifecycle.LifecycleOwner;
import com.zh.harmony.lifecycle.LifecycleRegistry;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityPackage;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * Class that provides lifecycle for the whole application process.
 * <p>
 * You can consider this LifecycleOwner as the composite of all of your Activities, except that
 * {@link Lifecycle.Event#ON_CREATE} will be dispatched once and {@link Lifecycle.Event#ON_DESTROY}
 * will never be dispatched. Other lifecycle events will be dispatched with following rules:
 * ProcessLifecycleOwner will dispatch {@link ohos.aafwk.ability.Lifecycle.Event#ON_START},
 * {@link Lifecycle.Event#ON_RESUME} events, as a first activity moves through these events.
 * {@link Lifecycle.Event#ON_PAUSE}, {@link ohos.aafwk.ability.Lifecycle.Event#ON_STOP}, events will be dispatched with
 * a <b>delay</b> after a last activity
 * passed through them. This delay is long enough to guarantee that ProcessLifecycleOwner
 * won't send any events if activities are destroyed and recreated due to a
 * configuration change.
 *
 * <p>
 * It is useful for use cases where you would like to react on your app coming to the foreground or
 * going to the background and you don't need a milliseconds accuracy in receiving lifecycle
 * events.
 */
@SuppressWarnings("WeakerAccess")
public class ProcessLifecycleOwner implements LifecycleOwner {
    private ActivityInitializationListener mProcessListener;

    static final long TIMEOUT_MS = 700; //mls

    // ground truth counters
    private int mStartedCounter = 0;
    private int mResumedCounter = 0;

    private boolean mPauseSent = true;
    private boolean mStopSent = true;

    private EventHandler mHandler;
    private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);

    private final Runnable mDelayedPauseRunnable = new Runnable() {
        @Override
        public void run() {
            dispatchPauseIfNeeded();
            dispatchStopIfNeeded();
        }
    };

    private final ActivityInitializationListener mInitializationListener
            = new ActivityInitializationListener() {
        @Override
        public void onCreate() {
        }

        @Override
        public void onStart() {
            activityStarted();
        }

        @Override
        public void onResume() {
            activityResumed();
        }
    };

    private static final ProcessLifecycleOwner sInstance = new ProcessLifecycleOwner();

    /**
     * The LifecycleOwner for the whole application process. Note that if your application
     * has multiple processes, this provider does not know about other processes.
     *
     * @return {@link LifecycleOwner} for the whole application.
     */
    public static LifecycleOwner get() {
        return sInstance;
    }

    public static void init(AbilityPackage application) {
        sInstance.attach(application);
    }

    void activityStarted() {
        mStartedCounter++;
        if (mStartedCounter == 1 && mStopSent) {
            mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START);
            mStopSent = false;
        }
    }

    void activityResumed() {
        mResumedCounter++;
        if (mResumedCounter == 1) {
            if (mPauseSent) {
                mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
                mPauseSent = false;
            } else {
                mHandler.removeTask(mDelayedPauseRunnable);
            }
        }
    }

    void activityPaused() {
        mResumedCounter--;
        if (mResumedCounter == 0) {
            mHandler.postTask(mDelayedPauseRunnable, TIMEOUT_MS);
        }
    }

    void activityStopped() {
        mStartedCounter--;
        dispatchStopIfNeeded();
    }

    void dispatchPauseIfNeeded() {
        if (mResumedCounter == 0) {
            mPauseSent = true;
            mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
        }
    }

    void dispatchStopIfNeeded() {
        if (mStartedCounter == 0 && mPauseSent) {
            mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);
            mStopSent = true;
        }
    }

    private ProcessLifecycleOwner() {
    }

    void attach(AbilityPackage application) {
        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
        application.registerCallbacks(new EmptyActivityLifecycleCallbacks() {
            @Override
            public void onAbilityStart(Ability ability) {
                super.onAbilityStart(ability);
                dispatchCreate(mProcessListener);
            }

            @Override
            public void onAbilityActive(Ability ability) {
                super.onAbilityActive(ability);
                dispatchStart(mProcessListener);
            }

            @Override
            public void onAbilityInactive(Ability ability) {
                super.onAbilityInactive(ability);
                activityPaused();
            }

            @Override
            public void onAbilityForeground(Ability ability) {
                super.onAbilityForeground(ability);
                dispatchResume(mProcessListener);
            }

            @Override
            public void onAbilityBackground(Ability ability) {
                super.onAbilityBackground(ability);
                activityStopped();
            }

            @Override
            public void onAbilityStop(Ability ability) {
                super.onAbilityStop(ability);
            }
        }, null);
        setProcessListener(mInitializationListener);
    }

    @Override
    public Lifecycle getLifeCycle() {
        return mRegistry;
    }

    public interface ActivityInitializationListener {
        void onCreate();

        void onStart();

        void onResume();
    }

    private void setProcessListener(ActivityInitializationListener processListener) {
        this.mProcessListener = processListener;
    }

    private void dispatchCreate(ActivityInitializationListener listener) {
        if (listener != null) {
            listener.onCreate();
        }
    }

    private void dispatchStart(ActivityInitializationListener listener) {
        if (listener != null) {
            listener.onStart();
        }
    }

    private void dispatchResume(ActivityInitializationListener listener) {
        if (listener != null) {
            listener.onResume();
        }
    }
}