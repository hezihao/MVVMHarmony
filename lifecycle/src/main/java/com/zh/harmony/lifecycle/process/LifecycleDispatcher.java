/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zh.harmony.lifecycle.process;

import com.zh.harmony.lifecycle.ReportFragment;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityPackage;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * When initialized, it hooks into the Activity callback of the Application and observes
 * Activities. It is responsible to hook in child-fragments to activities and fragments to report
 * their lifecycle events. Another responsibility of this class is to mark as stopped all lifecycle
 * providers related to an activity as soon it is not safe to run a fragment transaction in this
 * activity.
 */
public class LifecycleDispatcher {
    private static final AtomicBoolean sInitialized = new AtomicBoolean(false);

    public static void init(AbilityPackage application) {
        if (sInitialized.getAndSet(true)) {
            return;
        }
        application.registerCallbacks(new DispatcherActivityCallback(), null);
    }

    @SuppressWarnings("WeakerAccess")
    static class DispatcherActivityCallback extends EmptyActivityLifecycleCallbacks {
        @Override
        public void onAbilityStart(Ability ability) {
            super.onAbilityStart(ability);
            ReportFragment.injectIfNeededIn(ability);
        }
    }

    private LifecycleDispatcher() {
    }
}