/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zh.harmony.lifecycle;

/**
 * 生成的适配器需要实现的接口
 */
public interface GeneratedAdapter {
    /**
     * 生命周期状态发生改变时，回调该方法
     *
     * @param source 事件源
     * @param event  事件
     * @param onAny  approveCall onAny handlers
     * @param logger 如果传入，则用于跟踪被调用的方法，并防止该方法只被调用1次
     */
    void callMethods(LifecycleOwner source, Lifecycle.Event event, boolean onAny,
                     MethodCallsLogger logger);
}