package com.zh.harmony.lifecycle;

/**
 * Java8方式的订阅者
 */
public interface DefaultLifecycleObserver extends FullLifecycleObserver {
    @Override
    default void onCreate(LifecycleOwner owner) {
    }

    @Override
    default void onStart(LifecycleOwner owner) {
    }

    @Override
    default void onResume(LifecycleOwner owner) {
    }

    @Override
    default void onPause(LifecycleOwner owner) {
    }

    @Override
    default void onStop(LifecycleOwner owner) {
    }

    @Override
    default void onDestroy(LifecycleOwner owner) {
    }
}