/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zh.harmony.lifecycle;


import java.util.HashMap;
import java.util.Map;

/**
 * @hide
 */
public class MethodCallsLogger {
    /**
     * 调用过的方法缓存在这个Map中，如果调用过1次，则key为方法名，value值有值
     */
    private Map<String, Integer> mCalledMethods = new HashMap<>();

    /**
     * 是否允许调用
     *
     * @param name 准备回调的订阅者方法
     */
    public boolean approveCall(String name, int type) {
        Integer nullableMask = mCalledMethods.get(name);
        //如果没有被调用过，返回0
        int mask = nullableMask != null ? nullableMask : 0;
        //mask和type都为1时，返回1，如果有其中一个为0，则返回0，不为0是，才允许被调用
        boolean wasCalled = (mask & type) != 0;
        //计数
        mCalledMethods.put(name, mask | type);
        return !wasCalled;
    }
}